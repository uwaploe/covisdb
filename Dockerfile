FROM alpine:latest
MAINTAINER Mike Kenney <mikek@apl.uw.edu>

RUN mkdir /app
WORKDIR /app
COPY covisdb /app

ARG VERSION
ARG BUILD_DATE
ARG VCS_REF

LABEL org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.version=$VERSION \
      org.label-schema.vcs-ref=$VCS_REF

ENTRYPOINT ["./covisdb"]
