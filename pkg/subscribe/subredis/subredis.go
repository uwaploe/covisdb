// Package subredis subscribes (asynchronously) to one or more Redis
// pub-sub channels.
package subredis

import (
	"bitbucket.org/uwaploe/covisdb/pkg/subscribe"
	"github.com/garyburd/redigo/redis"
)

// Package logging function
var LogFunc func(string, ...interface{})

type Subscriber struct {
	conn redis.PubSubConn
}

// New creates a new Subscriber instance from a Redis connection.
func New(conn redis.Conn) *Subscriber {
	s := &Subscriber{
		conn: redis.PubSubConn{Conn: conn},
	}
	return s
}

// Subscribe implements the Subscriber interface.
func (s *Subscriber) Subscribe(topic ...string) <-chan subscribe.Message {
	ch := make(chan subscribe.Message, 1)
	go func() {
		defer close(ch)
		for {
			switch msg := s.conn.Receive().(type) {
			case error:
				if LogFunc != nil {
					LogFunc("Error: %v", msg)
				}
				return
			case redis.Subscription:
				if msg.Count == 0 {
					if LogFunc != nil {
						LogFunc("Pubsub channel closed")
					}
					return
				} else {
					if LogFunc != nil {
						LogFunc("Subscribed to %q", msg.Channel)
					}
				}
			case redis.Message:
				ch <- subscribe.Message{Topic: msg.Channel, Contents: msg.Data}
			case redis.PMessage:
				ch <- subscribe.Message{Topic: msg.Channel, Contents: msg.Data}
			}
		}
	}()

	channels := make([]interface{}, 0, len(topic))
	for _, t := range topic {
		channels = append(channels, t)
	}
	s.conn.PSubscribe(channels...)
	return ch
}

// Unsubscribe implements the Subscriber interface.
func (s *Subscriber) Unsubscribe(topic ...string) {
	channels := make([]interface{}, 0, len(topic))
	for _, t := range topic {
		channels = append(channels, t)
	}
	s.conn.PUnsubscribe(channels...)
}
