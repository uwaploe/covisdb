// Package subfiles treats a series of files as topics which can be
// subscribed to.
package subfiles

import (
	"bufio"
	"context"
	"io"
	"os"
	"sync"

	"bitbucket.org/uwaploe/covisdb/pkg/subscribe"
)

// Package logging function
var LogFunc func(string, ...interface{})

type Subscriber struct {
	files  map[string]io.ReadCloser
	ctx    context.Context
	cancel context.CancelFunc
}

func New(src map[string]string) *Subscriber {
	s := Subscriber{
		files: make(map[string]io.ReadCloser),
	}
	for k, v := range src {
		file, err := os.Open(v)
		if err == nil {
			s.files[k] = file
		} else {
			if LogFunc != nil {
				LogFunc("Cannot open %q: %v", v, err)
			}
		}
	}

	return &s
}

func (s *Subscriber) Subscribe(topic ...string) <-chan subscribe.Message {
	ch := make(chan subscribe.Message, len(s.files))
	wg := sync.WaitGroup{}

	s.ctx, s.cancel = context.WithCancel(context.Background())

	for name, rdr := range s.files {
		wg.Add(1)
		go func(ctx context.Context, topic string, r io.ReadCloser) {
			defer wg.Done()
			defer r.Close()
			scanner := bufio.NewScanner(r)
			for scanner.Scan() {
				// We need to copy the return value from scanner.Bytes as
				// it will be reused on subsequent iterations
				ch <- subscribe.Message{
					Topic:    topic,
					Contents: append([]byte(nil), scanner.Bytes()...)}
				select {
				case <-ctx.Done():
					return
				default:
				}
			}
		}(s.ctx, name, rdr)
	}

	go func() {
		defer close(ch)
		wg.Wait()
	}()

	return ch
}

func (s *Subscriber) Unsubscribe(topic ...string) {
	if s.cancel != nil {
		s.cancel()
	}
}
