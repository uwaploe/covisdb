// Package subscribe provides an interface for subscribing to messages
// published by a message broker.
package subscribe

type Message struct {
	Topic    string
	Contents []byte
}

type Subscriber interface {
	// Subscribe to one or more topics. The returned channel
	// is used to receive messages. The channel is closed when
	// all topics are unsubscribed.
	Subscribe(topic ...string) <-chan Message
	// Unsubscribe from one or more topics or all of them if
	// no arguments are passed.
	Unsubscribe(topic ...string)
}
