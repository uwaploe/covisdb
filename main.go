// Copyright © 2018 Michael Kenney <mikek@apl.uw.edu>

package main

import "bitbucket.org/uwaploe/covisdb/cmd"

var Version = "dev"
var BuildDate = "unknown"

func main() {
	cmd.Execute(Version)
}
