DATE    ?= $(shell date +%FT%T%z)
VERSION ?= $(shell git describe --tags --always --dirty --match=v* 2> /dev/null || \
			cat $(CURDIR)/.version 2> /dev/null || echo v0)
VCS_REF := $(shell git log -1 --pretty=%h)

ifdef LINUX32
GOBUILD := CGO_ENABLED=0 GOOS=linux GOARCH=386 go build
else
GOBUILD := CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build
endif
APP := covisdb
VMHOST ?= sysop@atrs2

all: $(APP)

dep:
	@go get -v -d ./...

$(APP): dep
	@$(GOBUILD) -v -o $@ \
	  -tags release \
	  -ldflags '-s -X main.Version=$(VERSION) -X main.BuildDate=$(DATE)'

image: $(APP)
	docker build -t $(APP):latest --build-arg VERSION="${VERSION}" \
	--build-arg BUILD_DATE="${DATE}" \
	--build-arg VCS_REF="${VCS_REF}" .

install: 
	docker save $(APP):latest | bzip2 | ssh $(VMHOST) docker load

clean:
	@rm -f $(APP)
