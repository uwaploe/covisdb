// Copyright © 2018 Michael Kenney <mikek@apl.uw.edu>

package cmd

import (
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"bitbucket.org/uwaploe/covisdb/pkg/subscribe/subfiles"
	"github.com/spf13/cobra"
)

// loadCmd represents the load command
var loadCmd = &cobra.Command{
	Use:   "load file,topic [file,topic ...]",
	Args:  cobra.MinimumNArgs(1),
	Short: "Load the contents of one or more files",
	Long: `Load the contents of one or more newline-delimited
JSON files into the database. Each line of the file is the contents
of a message from the COVIS system. Each file is associated with
a single "topic" which must be specified. Valid topics are:

covis.sweep
covis.ping
covis.move
covis.therm
`,
	SilenceUsage: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		cln, err := dbClient()
		if err != nil {
			return err
		}

		files := make(map[string]string)
		for _, arg := range args {
			pair := strings.Split(arg, ",")
			files[pair[1]] = pair[0]
		}

		sigs := make(chan os.Signal, 1)
		signal.Notify(sigs, syscall.SIGINT,
			syscall.SIGTERM, syscall.SIGHUP)
		defer signal.Stop(sigs)

		sub := subfiles.New(files)

		go func() {
			s, more := <-sigs
			if more {
				log.Printf("Got signal: %v. Exiting.", s)
				sub.Unsubscribe()
			}
		}()

		return transferData(sub, cln, 100)
	},
}

func init() {
	rootCmd.AddCommand(loadCmd)
}
