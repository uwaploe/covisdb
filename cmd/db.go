package cmd

import (
	"path/filepath"
	"time"

	"bitbucket.org/uwaploe/go-covis/pkg/sweep"
	"github.com/influxdata/influxdb/client/v2"
)

// Angle represents a rotation of the platform
type Angle struct {
	Roll    float64 `json:"roll"`
	Pitch   float64 `json:"pitch"`
	Heading float64 `json:"heading"`
}

// Rotator indices
const (
	ROLL int = 0
	TILT int = 1
	PAN  int = 2
)

// State contains the current orientation state of the platform
type State struct {
	T time.Time `json:"t"`
	// Angle of each Rotator in their own coordinate system
	Rotation [3]float64 `json:"rotation"`
	Stalled  uint       `json:"stall_flag"`
	Angle
}

// Thermistor data record
type Thermistors struct {
	Timestamp time.Time      `json:"t"`
	Data      map[string]int `json:"data"`
}

// Sweep error
type SweepError struct {
	Timestamp time.Time   `json:"t"`
	Text      string      `json:"error"`
	Ctx       sweep.Sweep `json:"sweep"`
}

// Create a set of Points from a State struct
func statePoints(s State) ([]*client.Point, error) {
	p0, err := client.NewPoint("rotator", nil,
		map[string]interface{}{
			"roll": s.Rotation[ROLL],
			"tilt": s.Rotation[TILT],
			"pan":  s.Rotation[PAN]},
		s.T)
	if err != nil {
		return nil, err
	}

	p1, err := client.NewPoint("attitude", nil,
		map[string]interface{}{
			"roll":    s.Roll,
			"pitch":   s.Pitch,
			"heading": s.Heading},
		s.T)
	if err != nil {
		return nil, err
	}

	return []*client.Point{p0, p1}, nil
}

// Create a set of Points from a Sweep
func sweepPoints(sw sweep.Sweep) ([]*client.Point, error) {
	id := filepath.Base(sw.Dir)
	pings := int(sw.Pings)
	if sw.Stops != nil {
		steps := sw.Stops.Steps
		if sw.Stops.Roundtrip {
			steps += (sw.Stops.Steps - 1)
		}
		pings *= steps
	}

	p0, err := client.NewPoint("sweeps",
		map[string]string{
			"type": sw.Type,
		},
		map[string]interface{}{
			"event": "start",
			"id":    id,
			"pings": pings,
		},
		sw.Tstart)
	if err != nil {
		return nil, err
	}

	p1, err := client.NewPoint("sweeps",
		map[string]string{
			"type": sw.Type,
		},
		map[string]interface{}{
			"event": "stop",
			"id":    id,
			"pings": pings,
		},
		sw.Tend)
	if err != nil {
		return nil, err
	}

	return []*client.Point{p0, p1}, nil
}

func thermPoints(th Thermistors) ([]*client.Point, error) {
	points := make([]*client.Point, 0, 3)
	for _, id := range []string{"A", "B", "C"} {
		p, err := client.NewPoint("thermistors",
			map[string]string{
				"id": id,
			},
			map[string]interface{}{
				"temperature": th.Data[id],
			},
			th.Timestamp)
		if err != nil {
			return nil, err
		}
		points = append(points, p)
	}

	return points, nil
}

func errorPoint(se SweepError) ([]*client.Point, error) {
	id := filepath.Base(se.Ctx.Dir)
	p, err := client.NewPoint("errors",
		nil,
		map[string]interface{}{
			"text":  se.Text,
			"sweep": id,
		},
		se.Timestamp)
	return []*client.Point{p}, err
}
