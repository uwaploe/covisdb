// Copyright © 2018 Michael Kenney <mikek@apl.uw.edu>

package cmd

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"

	"bitbucket.org/uwaploe/covisdb/pkg/subscribe"
	"bitbucket.org/uwaploe/go-covis/pkg/sweep"
	"github.com/influxdata/influxdb/client/v2"
	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	cfgFile string
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "covisdb",
	Short: "Add records to the COVIS InfluxDB database",
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute(version string) {
	rootCmd.Version = version
	if err := rootCmd.Execute(); err != nil {
		log.Fatalf("Error: %v", err)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "",
		"config file (default is $HOME/.covisdb.yaml)")
	rootCmd.PersistentFlags().String("dburl", "http://localhost:8086",
		"InfluxDB URL")
	rootCmd.PersistentFlags().String("dbname", "covis", "InfluxDB database name")
	rootCmd.PersistentFlags().String("dbcreds", "",
		"InfluxDB database credentials USER:PASSWORD")
	for _, name := range []string{"dburl", "dbname", "dbcreds"} {
		viper.BindPFlag(name, rootCmd.PersistentFlags().Lookup(name))
	}
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// Search config in home directory
		viper.AddConfigPath(home)
		viper.SetConfigName(".covisdb")
	}

	viper.SetEnvPrefix("covisdb")
	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}

func dbClient() (client.Client, error) {
	cfg := client.HTTPConfig{
		Addr: viper.GetString("dburl"),
	}

	dbcreds := viper.GetString("dbcreds")
	if dbcreds != "" {
		parts := strings.Split(dbcreds, ":")
		cfg.Username = parts[0]
		cfg.Password = parts[1]
	}

	return client.NewHTTPClient(cfg)
}

func writeBatch(cln client.Client, bp client.BatchPoints) <-chan error {
	ch := make(chan error, 1)
	go func() {
		ch <- cln.Write(bp)
	}()

	return ch
}

func transferData(src subscribe.Subscriber, cln client.Client, batchSize int) error {
	ch := src.Subscribe("covis.*")
	count := int(0)

	var bp client.BatchPoints
	var future <-chan error
	for msg := range ch {
		if count == 0 {
			bp, _ = client.NewBatchPoints(client.BatchPointsConfig{
				Database: viper.GetString("dbname"),
			})
		}

		switch msg.Topic {
		case "covis.sweep":
			sw := sweep.Sweep{}
			err := json.Unmarshal(msg.Contents, &sw)
			if err == nil {
				points, err := sweepPoints(sw)
				if err == nil {
					bp.AddPoints(points)
					count += len(points)
				} else {
					log.Printf("Point creation error: %v", err)
				}
			} else {
				log.Printf("JSON decode error: %v", err)
			}
		case "covis.move":
			st := State{}
			err := json.Unmarshal(msg.Contents, &st)
			if err == nil {
				points, err := statePoints(st)
				if err == nil {
					bp.AddPoints(points)
					count += len(points)
				} else {
					log.Printf("Point creation error: %#v", err)
				}
			} else {
				log.Printf("JSON decode error: %#v", err)
			}
		case "covis.therm":
			th := Thermistors{}
			err := json.Unmarshal(msg.Contents, &th)
			if err == nil {
				points, err := thermPoints(th)
				if err == nil {
					bp.AddPoints(points)
					count += len(points)
				} else {
					log.Printf("Point creation error: %#v", err)
				}
			} else {
				log.Printf("Point creation error: %#v", err)
			}
		case "covis.error":
			se := SweepError{}
			err := json.Unmarshal(msg.Contents, &se)
			if err == nil {
				points, err := errorPoint(se)
				if err == nil {
					bp.AddPoints(points)
					count += len(points)
				} else {
					log.Printf("Point creation error: %#v", err)
				}
			} else {
				log.Printf("Point creation error: %#v", err)
			}
		}

		if count >= batchSize {
			// Check the status of the previous write
			if future != nil {
				if err := <-future; err != nil {
					return err
				} else {
					log.Printf("Wrote %d points to database", count)
				}
			}
			// Asynchronously write the batch of points
			future = writeBatch(cln, bp)
			count = 0
		}
	}

	if count != 0 {
		if err := cln.Write(bp); err != nil {
			return err
		}
		log.Printf("Wrote %d points to database", count)
	}

	return nil
}
