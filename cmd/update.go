// Copyright © 2018 Michael Kenney <mikek@apl.uw.edu>

package cmd

import (
	"log"
	"os"
	"os/signal"
	"syscall"

	"bitbucket.org/uwaploe/covisdb/pkg/subscribe/subredis"
	"github.com/garyburd/redigo/redis"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// updateCmd represents the update command
var updateCmd = &cobra.Command{
	Use:   "update HOST:PORT",
	Args:  cobra.ExactArgs(1),
	Short: "Update COVIS database with contents of Redis messages",
	Long: `Subscribe to the covis.* channels for messages published by
the COVIS System Interface Controller and store the contents in an InfluxDB
database.
`,
	SilenceUsage: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		conn, err := redis.Dial("tcp", args[0])
		if err != nil {
			return err
		}

		cln, err := dbClient()
		if err != nil {
			return err
		}

		sigs := make(chan os.Signal, 1)
		signal.Notify(sigs, syscall.SIGINT,
			syscall.SIGTERM, syscall.SIGHUP)
		defer signal.Stop(sigs)

		sub := subredis.New(conn)

		go func() {
			s, more := <-sigs
			if more {
				log.Printf("Got signal: %v. Exiting.", s)
				sub.Unsubscribe()
			}
		}()

		subredis.LogFunc = log.Printf
		return transferData(sub, cln, viper.GetInt("batch"))
	},
}

func init() {
	rootCmd.AddCommand(updateCmd)
	updateCmd.PersistentFlags().Int("batch", 10, "Minimum number of points to upload")
	viper.BindPFlag("batch", updateCmd.PersistentFlags().Lookup("batch"))
}
