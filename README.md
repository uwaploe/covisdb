# COVIS Engineering Database

The `covisdb` application is used to store the COVIS engineering data in
an InfluxDB database.

| **Measurement** | **Tags** | **Fields**           |
|--------------- |-------- |-------------------- |
| sweeps          | type     | event, id, pings     |
| rotator         | -        | roll, tilt, pan      |
| attitude        | -        | roll, pitch, heading |
| thermistors     | id       | temperature          |
| errors          | -        | text, sweep          |
